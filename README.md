# GIMP-Scripts

This is a test repository for experimenting with a possible replacement for registry.gimp.org.

There are some git repos representing specific scripts [patdavid-check-layer](https://gitlab.com/GIMP/patdavid-check-layer) that is a repo for this account.

There are some git repos that are from Github that are also being included as a submodule.  
[akkana-gimp-plugins](https://github.com/akkana/gimp-plugins) is one we are testing with from Github for example.


## Git Submodules

If each GIMP script is housed in it's own git repository, then we can simply link in each repo as a submodule to this repository directly.
